package lighthouse_2;

import java.util.ArrayList;
import java.util.Scanner;

public class AppLightHouse_2 {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        ReadCSV obj = new ReadCSV();
        int menu = 1;
        int menu2;

        do {
            System.out.println("\nDeseja ler um arquivo csv(1)\n"
                    + "Deseja reverter a ordem de um array(2)\n"
                    + "EXIT (0)");
            menu2 = sc.nextInt();

            switch (menu2) {

                default:
                    System.out.println("Invalid option!");
                    break;

                case 0:
                    System.out.println("\nBYE!");
                    menu = 0;
                    break;

                case 1:

                    System.out.println("\nCSV file view (fourth exercise)\n");
                    System.out.println("Insert adress of CSV file");
                    obj.run(sc.next());
                    break;

                case 2:
                    System.out.print("Delimit the size of Array: ");
                    Structure t1 = new Structure(sc.nextInt());
                    int menu3;
                    int i = 0;

                    while (!t1.isFull()) {
                        System.out.println("Type the " + t1.ordinal(i + 1) + " number");
                        t1.enqueue(sc.nextInt());
                        i++;
                    }

                    do {

                        System.out.println("\nPress the number of menu option, after press enter to interact\n");

                        System.out.println("Reverse Array order (First exercise): (1)");
                        System.out.println("Reverse Array order (Secound exercise): (2)");
                        System.out.println("Reverse Array order (third exercise): (3)");
                        System.out.println("CSV file view (fourth exercise): (4)");
                        System.out.println("BACK: (0)\n");

                        menu3 = sc.nextInt();

                        switch (menu3) {
                            case 1:
                                System.out.println(t1.exercise1());
                                break;
                            case 2:
                                System.out.println(t1.exercise2());
                                break;
                            case 3:
                                ArrayList<Integer> arr = new ArrayList();
                                t1.exercise3(arr);
                                for (int j = 0; j < arr.size(); j++) {
                                    System.out.print(arr.get(j) + ", ");
                                }
                                break;
                            case 4:
                                System.out.println("Insert adress of CSV file");
                                obj.run(sc.next());
                                System.out.println("");
                                break;
                            case 0:
                                System.out.println("Returning...");
                                break;
                            default:
                                System.out.println("Invalid option!");
                                break;

                        }
                    } while (menu3 != 0);
                    break;
            }
        } while (menu != 0);
    }

}
