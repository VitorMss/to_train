package lighthouse_2;


import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class ReadCSV {

    public void run(String csvFile) {

        //"C:\\Users\\vitor\\OneDrive\\Documentos\\Excel\\Arquivo.csv";
        BufferedReader br = null;
        String linha = "";
        String csvDivisor = ", ";

        try {

            br = new BufferedReader(new FileReader(csvFile));
            while ((linha = br.readLine()) != null) {

                String[] pais = linha.split(csvDivisor);

                System.out.println("País [code= " + pais[pais.length - 1]
                        + " , name=" + pais[pais.length - 1] + "]");

            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
