package lighthouse_2;

import java.util.List;

public class Structure {

    private Structure1 p1;
    private Structure2 p2;

    public Structure(int sizeVet) {
        p1 = new Structure1(sizeVet);
        p2 = new Structure2(sizeVet);
    }

    public void enqueue(int value) {
        p1.enqueue(value);
    }

    public String ordinal(int i) {
        int mod100 = i % 100;
        int mod10 = i % 10;
        if (mod10 == 1 && mod100 != 11) {
            return i + "st";
        } else if (mod10 == 2 && mod100 != 12) {
            return i + "nd";
        } else if (mod10 == 3 && mod100 != 13) {
            return i + "rd";
        } else {
            return i + "th";
        }
    }

    public boolean isEmpty() {
        return p1.isEmpty();
    }

    public boolean isFull() {
        return p1.isFull();
    }

    public String print() {
        return p1.print();
    }

    public String exercise1() {
        Structure1 temp1 = p1;
        Structure2 temp2 = p2;
        String valueOfP2 = "";

        while (!p1.isEmpty()) {
            p2.push(p1.next());
            p1.dequeue();
        }
        while (!p2.isEmpty()) {
            valueOfP2 += p2.top() + " ";
            p2.pop();
        }

        return valueOfP2;
    }

    public String exercise2() {
        String rtn;
        Structure1 temp1 = p1;
        Structure2 temp2 = p2;
        while (!p1.isEmpty()) {
            p2.push(p1.next());
            p1.dequeue();
        }

        while (!p2.isEmpty()) {
            p1.enqueue(p2.top());
            p2.pop();
        }
        rtn = p1.print();
        return rtn;
    }

    public List exercise3(List<Integer> list) {
        Structure1 temp1 = p1;
        Structure2 temp2 = p2;

        while (!p1.isEmpty()) {
            p2.push(p1.next());
            p1.dequeue();
        }

        while (!p2.isEmpty()) {
            p1.enqueue(p2.top());
            list.add(p2.top());
            p2.pop();
        }
        return list;
    }
}
