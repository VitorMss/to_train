package lighthouse_2;

public class Structure1 {
    //FILA

    private int arr[];
    private int first, last, qnt;

    public Structure1(int sizeVet) {
        arr = new int[sizeVet];
        last = first = qnt = 0;

    }

    public void enqueue(int value) {
        if (!isFull()) {
            arr[last] = value;
            last = (last + 1) % arr.length;
            qnt++;
        }
    }

    public void dequeue() {
        if (!isEmpty()) {
            first = (first + 1) % arr.length;
            qnt--;
        }

    }

    public int next() {
        return arr[first];
    }

    public int size() {
        return qnt;
    }

    public boolean isEmpty() {
        return qnt == 0;
    }

    public boolean isFull() {
        return qnt == arr.length;
    }

    public String print() {
        String rtn = "";
        int imp = first;
        for (int i = 0; i < qnt; i++) {
            rtn = rtn + arr[imp] + ", ";
            imp = (imp + 1) % arr.length;
        }
        return rtn;
    }

}
