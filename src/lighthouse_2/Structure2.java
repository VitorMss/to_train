package lighthouse_2;

public class Structure2 {
    //PILHA

    int[] vet;
    int current;

    public Structure2(int sizeVet) {
        vet = new int[sizeVet];
        current = 0;
    }

    public void push(int dado) {
        if (current < vet.length) {
            vet[current] = dado;
            current++;
        }
    }

    public void pop() {
        if (!isEmpty()) {
            current--;
        }
    }

    public boolean isEmpty() {
        return current == 0;
    }

    public boolean isFull() {
        return current == vet.length;
    }

    public int size() {
        return current;
    }

    public int top() {
        return vet[current - 1];
    }

    public String print() {
        String value = "";
        for (int i = 0; i < current; i++) {
            value += vet[i] + " ";
        }
        return value;
    }
}
